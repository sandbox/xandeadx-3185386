<?php

namespace Drupal\entity_filter_searchapi;

use Drupal\entity_filter\EntityFilterList;
use Drupal\entity_filter_searchapi\Plugin\EntityFilter\SearchApiEntityFilterInterface;
use Drupal\search_api_solr\Event\PostSetFacetsEvent;
use Drupal\search_api_solr\Event\SearchApiSolrEvents;
use Drupal\search_api_solr\Plugin\search_api\backend\SearchApiSolrBackend;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SearchApiEntityFilterEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      SearchApiSolrEvents::POST_SET_FACETS => [
        ['onSearchApiSolrPostSetFacets_cloneFacetFields'],
        ['onSearchApiSolrPostSetFacets_createStatsFields'],
      ],
    ];
  }

  /**
   * SearchApiSolrEvents::POST_SET_FACETS event callback.
   *
   * Clone facet fields.
   */
  public function onSearchApiSolrPostSetFacets_cloneFacetFields(PostSetFacetsEvent $event): void {
    $searchapi_query = $event->getSearchApiQuery();
    $searchapi_query_entity_filters = new EntityFilterList($searchapi_query->getOption('entity_filters', []));

    $solr_query = $event->getSolariumQuery(); /** @var \Solarium\QueryType\Select\Query\Query $solr_query */
    $solr_facet_set = $solr_query->getFacetSet(); /** @var \Solarium\Component\FacetSet $solr_facet_set */
    $solr_facets = $solr_facet_set->getFacets();

    $solr_facet_set->setLimit(-1);

    $excludes = [];
    /** @var SearchApiEntityFilterInterface $searchapi_query_entity_filter */
    foreach ($searchapi_query_entity_filters as $searchapi_query_entity_filter) {
      if (!$searchapi_query_entity_filter->isGlobal()) {
        $excludes[] = 'facet:' . $searchapi_query_entity_filter->getFieldInfo('search_api_name');
      }
    }

    if ($excludes) {
      // Create new solr facet fields with suffix "_ex" and local params which contain exludes all solr query filters (fq) without global
      foreach ($solr_facets as $solr_field_name => $solr_facet) {
        /** @see \Drupal\search_api_solr\Plugin\search_api\backend\SearchApiSolrBackend::setFacets() */
        // @TODO Use clone() without createFacetField()
        $solr_facet_field_ex = $solr_facet_set->createFacetField($solr_field_name . '_ex')->setField($solr_field_name);
        $solr_facet_field_ex->getLocalParameters()->addExcludes($excludes);
      }
    }
  }

  /**
   * SearchApiSolrEvents::POST_SET_FACETS event callback.
   *
   * Create stats fields for "range" facets logic.
   */
  public function onSearchApiSolrPostSetFacets_createStatsFields(PostSetFacetsEvent $event): void {
    $searchapi_query = $event->getSearchApiQuery();
    $searchapi_index = $searchapi_query->getIndex();
    $searchapi_stats = $searchapi_query->getOption('search_api_stats', []);

    $solr_query = $event->getSolariumQuery(); /** @var \Solarium\QueryType\Select\Query\Query $solr_query */
    $solr_backend = $searchapi_index->getServerInstance()->getBackend(); /** @var SearchApiSolrBackend $solr_backend */
    $solr_field_names = $solr_backend->getSolrFieldNames($searchapi_index);
    $solr_stats = $solr_query->getStats(); /** @var \Solarium\Component\Stats\Stats $solr_stats */

    foreach ($searchapi_stats as $searchapi_stats_item) {
      $searchapi_stats_field_name = $searchapi_stats_item['field'];
      $solr_stats_field_name = $solr_field_names[$searchapi_stats_field_name];

      // Hack! Because \Solarium\Component\Stats\Field has no methods with set local params
      /** @see \Solarium\Component\RequestBuilder\Stats::buildComponent() */
      $solr_stats_field_name_with_local_params = $solr_query->getRequestBuilder()->renderLocalParams($solr_stats_field_name, [
        'ex' => 'facet:' . $searchapi_stats_field_name,
      ]);

      /** @var \Solarium\Component\Stats\Field $solr_stats_field */
      $solr_stats_field = $solr_stats
        ->createField($solr_stats_field_name)
        ->setKey($solr_stats_field_name_with_local_params);
    }
  }

}
