<?php

namespace Drupal\entity_filter_searchapi\Plugin\EntityFilter;

use Drupal\entity_filter\Plugin\EntityFilter\EntityFilterBase;
use Drupal\search_api\Query\QueryInterface as SearchApiQueryInterface;
use Drupal\search_api\Query\ResultSetInterface as SearchApiQueryResultInterface;
use Drupal\search_api_solr\Plugin\search_api\backend\SearchApiSolrBackend;

abstract class SearchApiEntityFilterBase extends EntityFilterBase implements SearchApiEntityFilterInterface {

  /**
   * {@inheritDoc}
   */
  public function buildFacetQuery(SearchApiQueryInterface $query): void {
    $this->callMethodWithPrefix('buildFacetQueryForFilterType', $this->getType(), ...func_get_args());
  }

  /**
   * Build facet query for filter type "value".
   */
  protected function buildFacetQueryForFilterTypeValue(SearchApiQueryInterface $query): void {
    $query_options = &$query->getOptions();
    $search_api_field_name = $this->getFieldInfo('search_api_name');

    /** @see \Drupal\facets\Plugin\facets\query_type\SearchApiString */
    $query_options['search_api_facets'][$search_api_field_name] = [
      'field' => $search_api_field_name,
      'limit' => -1,
      'operator' => $this->getOperator(),
      'min_count' => 1,
      'missing' => FALSE,
      'query_type' => 'search_api_string',
      'entity_filter' => $this,
    ];
  }

  /**
   * Build facet query for filter type "range".
   */
  protected function buildFacetQueryForFilterTypeRange(SearchApiQueryInterface $query): void {
    $query_options = &$query->getOptions();
    $search_api_field_name = $this->getFieldInfo('search_api_name');

    $query_options['search_api_stats'][$search_api_field_name] = [
      'field' => $search_api_field_name,
      'entity_filter' => $this,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getAllowedValues(SearchApiQueryResultInterface $results = NULL): array {
    return $this->callMethodWithPrefix('getAllowedValuesForFilterType', $this->getType(), ...func_get_args()) ?: [];
  }

  /**
   * Return allowed values for filter type "value".
   */
  protected function getAllowedValuesForFilterTypeValue(SearchApiQueryResultInterface $results = NULL): array {
    $allowed_values = [];

    if ($results) {
      $searchapi_facets_result = $results->getExtraData('search_api_facets');
      $filter_searchapi_field_name = $this->getFieldInfo('search_api_name');

      if (!empty($searchapi_facets_result[$filter_searchapi_field_name])) {
        foreach ($searchapi_facets_result[$filter_searchapi_field_name] as $facet_result) {
          $allowed_values[trim($facet_result['filter'], '"')] = $facet_result['count'];
        }
      }

      // Merge values from solr result
      /** @var \Solarium\Component\Result\FacetSet $solr_facets_result */
      if ($solr_facets_result = $results->getExtraData('facet_set')) {
        $searchapi_index = $results->getQuery()->getIndex();
        $solr_backend = $searchapi_index->getServerInstance()->getBackend(); /** @var SearchApiSolrBackend $solr_backend */
        $solr_field_names = $solr_backend->getSolrFieldNames($searchapi_index);
        $solr_field_name = $solr_field_names[$filter_searchapi_field_name];

        /** @var \Solarium\Component\Result\Facet\Field $solr_facet_result */
        if ($solr_facet_result = $solr_facets_result->getFacet($solr_field_name . '_ex')) {
          $solr_facet_result_values = array_fill_keys(array_keys($solr_facet_result->getValues()), 0);
          $allowed_values += $solr_facet_result_values;
        }
      }

      // Sort by keys
      ksort($allowed_values, SORT_NATURAL | SORT_FLAG_CASE);
    }

    return $allowed_values;
  }

  /**
   * Return allowed values for filter type "range".
   */
  protected function getAllowedValuesForFilterTypeRange(SearchApiQueryResultInterface $results = NULL): array {
    $allowed_values = [];

    if ($results) {
      $searchapi_query = $results->getQuery();
      $searchapi_index = $searchapi_query->getIndex();
      $filer_searchapi_field_name = $this->getFieldInfo('search_api_name');
      $solr_response = $results->getExtraData('search_api_solr_response');
      $solr_backend = $searchapi_index->getServerInstance()->getBackend(); /** @var SearchApiSolrBackend $solr_backend */
      $solr_field_names = $solr_backend->getSolrFieldNames($searchapi_index);
      $allowed_values = $solr_response['stats']['stats_fields'][$solr_field_names[$filer_searchapi_field_name]];
    }

    return $allowed_values;
  }

  /**
   * {@inheritDoc}
   */
  public function processConditionValues(array $values): array {
    if ($this->getType() == 'range') {
      if (!isset($values['min']) || $values['min'] === '') {
        $values['min'] = PHP_INT_MIN;
      }
      if (!isset($values['max']) || $values['max'] === '') {
        $values['max'] = PHP_INT_MAX;
      }
    }

    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function applyCondition(SearchApiQueryInterface $query = NULL): void {
    if ($this->hasValues()) {
      $this->callMethodWithPrefix('applyConditionForFilterType', $this->getType(), ...func_get_args());

      $query_options = &$query->getOptions();
      $query_options['entity_filters'][$this->id()] = $this;
    }
  }

  /**
   * Apply condition for filter type "value".
   */
  protected function applyConditionForFilterTypeValue(SearchApiQueryInterface $query = NULL): void {
    $searchapi_field_name = $this->getFieldInfo('search_api_name');
    $values = $this->processConditionValues($this->getValues());

    /** @see \Drupal\facets\Plugin\facets\query_type\SearchApiString::execute() */
    $filter = $query->createConditionGroup($this->getOperator(), ['facet:' . $searchapi_field_name]);
    foreach ($values as $value) {
      $filter->addCondition($searchapi_field_name, $value);
    }
    $query->addConditionGroup($filter);
  }

  /**
   * Apply condition for filter type "range".
   */
  protected function applyConditionForFilterTypeRange(SearchApiQueryInterface $query = NULL): void {
    $searchapi_field_name = $this->getFieldInfo('search_api_name');
    $values = $this->processConditionValues($this->getValues());

    /** @see \Drupal\facets\Plugin\facets\query_type\SearchApiRange::execute() */
    $filter = $query->createConditionGroup($this->getOperator(), ['facet:' . $searchapi_field_name]);
    $filter->addCondition($searchapi_field_name, [$values['min'], $values['max']], 'BETWEEN');
    $query->addConditionGroup($filter);
  }

  /**
   * Apply condition for filter type "search".
   */
  protected function applyConditionForFilterTypeSearch(SearchApiQueryInterface $query = NULL): void {
    $searchapi_field_name = $this->getFieldInfo('search_api_name');
    $filter_values = $this->processConditionValues($this->getValues());

    // See https://niklan.net/blog/176#primer-no2-plagin-parsinga
    $parse_mode_plugin_manager = \Drupal::service('plugin.manager.search_api.parse_mode');
    $parse_mode_plugin = $parse_mode_plugin_manager->createInstance('terms');
    $parse_mode_plugin->setConjunction('AND');
    $query->setParseMode($parse_mode_plugin);

    $query->keys(current($filter_values));

    if ($searchapi_field_name) {
      $query->setFulltextFields([$searchapi_field_name]);
    }
  }

}
