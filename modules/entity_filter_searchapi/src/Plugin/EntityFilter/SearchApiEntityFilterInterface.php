<?php

namespace Drupal\entity_filter_searchapi\Plugin\EntityFilter;

use Drupal\entity_filter\Plugin\EntityFilter\EntityFilterInterface;
use Drupal\search_api\Query\QueryInterface as SearchApiQueryInterface;
use Drupal\search_api\Query\ResultSetInterface as SearchApiResultsInterface;

interface SearchApiEntityFilterInterface extends EntityFilterInterface {

  /**
   * Build facets query.
   */
  public function buildFacetQuery(SearchApiQueryInterface $query): void;

  /**
   * {@inheritDoc}
   */
  public function getAllowedValues(SearchApiResultsInterface $results = NULL): array;

  /**
   * {@inheritDoc}
   */
  public function applyCondition(SearchApiQueryInterface $query = NULL): void;

}
