<?php

namespace Drupal\entity_filter_searchapi;

use Drupal\entity_filter\EntityFilterManagerInterface;
use Drupal\search_api\Query\QueryInterface as SearchApiQueryInterface;

interface SearchApiEntityFilterManagerInterface extends EntityFilterManagerInterface {

  /**
   * {@inheritDoc}
   */
  public function getQuery(): SearchApiQueryInterface;

}
