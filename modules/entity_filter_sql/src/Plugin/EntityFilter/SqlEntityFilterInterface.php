<?php

namespace Drupal\entity_filter_sql\Plugin\EntityFilter;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\entity_filter\EntityFilterListInterface;
use Drupal\entity_filter\Plugin\EntityFilter\EntityFilterInterface;

interface SqlEntityFilterInterface extends EntityFilterInterface {

  /**
   * {@inheritDoc}
   */
  public function applyCondition(SelectInterface $query = NULL): void;

  /**
   * {@inheritDoc}
   */
  public function getAllowedValues(EntityFilterListInterface $filters = NULL, SelectInterface $query = NULL): array;

  /**
   * Join table.
   */
  public function joinTable(SelectInterface $query): string;

  /**
   * Return query table aliases.
   */
  public function getTableAliasesFromQuery(SelectInterface $query, string $table_name): array;

  /**
   * Return query table alias.
   */
  public function getTableAliasFromQuery(SelectInterface $query, string $table_name): ?string;

  /**
   * Create table alias with prefix.
   */
  public function createTableAlias(string $table_name, string $alias_prefix): string;

}
