<?php

namespace Drupal\entity_filter_sql\Plugin\EntityFilter;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\entity_filter\EntityFilterListInterface;
use Drupal\entity_filter\Plugin\EntityFilter\EntityFilterBase;
use Drupal\entity_filter\Plugin\EntityFilter\SqlEntityFilterInterface;

abstract class SqlEntityFilterBase extends EntityFilterBase implements SqlEntityFilterInterface {

  /**
   * {@inheritDoc}
   */
  public function joinTable(SelectInterface $query): string {
    $field_info = $this->getFieldInfo();
    $base_table_alias = $query->getMetaData('base_table_alias');

    if ($field_info && $field_info['property']) {
      $base_table_primary_key = $query->getMetaData('base_table_primary_key');
      return $query->innerJoin($field_info['table'], NULL, "%alias.entity_id = $base_table_alias.$base_table_primary_key");
    }

    return $base_table_alias;
  }

  /**
   * {@inheritDoc}
   */
  public function getAllowedValues(EntityFilterListInterface $filters = NULL, SelectInterface $query = NULL, float &$time = NULL): array {
    $this->timerStart($time);
    $allowed_values = $this->callMethodWithPrefix('getAllowedValuesForFIlterType', $this->getType(), ...func_get_args());
    $this->timerStop($time);
    return $allowed_values;
  }

  /**
   * Return allowed values for filter type "value".
   */
  protected function getAllowedValuesForFIlterTypeValue(SelectInterface $query, EntityFilterListInterface $filters): array {
    $field_info = $this->getFieldInfo();

    if (!$field_info) {
      return [];
    }

    // Get all allowed values. Needs because next query return only filled values.
    $query_without_filters = clone $query;
    $query_without_filters->distinct();
    $field_table_alias = $this->joinTable($query_without_filters);
    $query_without_filters->addField($field_table_alias, $field_info['column'], 'value');
    $query_without_filters->isNotNull("$field_table_alias.{$field_info['column']}");

    if ($depends_on = $this->getDependsOn()) {
      foreach ($filters as $filter_id => $filter) {
        if (in_array($filter_id, $depends_on)) {
          $filter->applyCondition($query_without_filters);
        }
      }
    }

    $allowed_values = array_fill_keys($query_without_filters->execute()->fetchCol(), 0);

    // Get allowed values considering other filters
    if ($allowed_values) {
      $query_with_filters = clone $query;

      $filters->applyConditions($query_with_filters, [$this->id()]);

      $field_table_alias = $this->joinTable($query_with_filters);
      $query_with_filters->addField($field_table_alias, $field_info['column'], 'value');
      $query_with_filters->addExpression('COUNT(*)', 'counter');
      $query_with_filters->isNotNull("$field_table_alias.{$field_info['column']}");
      $query_with_filters->groupBy("$field_table_alias.{$field_info['column']}");
      $allowed_values = $query_with_filters->execute()->fetchAllKeyed() + $allowed_values;
    }

    return $allowed_values;
  }

  /**
   * Return allowed values for filter type "boolean".
   */
  protected function getAllowedValuesForFIlterTypeBoolean(SelectInterface $query, EntityFilterListInterface $filters): array {
    $field_info = $this->getFieldInfo();

    if (!$field_info) {
      return [];
    }

    $query_with_filters = clone $query;

    $filters = clone $filters;
    $filters[$this->id()] = clone $this;
    $filters[$this->id()]->setValues([1]);
    $filters->applyConditions($query_with_filters);

    $base_table_alias = $query->getMetaData('base_table_alias');
    $base_table_primary_key = $query->getMetaData('base_table_primary_key');
    $query_with_filters->addExpression("COUNT(DISTINCT $base_table_alias.$base_table_primary_key)", 'counter');

    return [1 => $query_with_filters->execute()->fetchField()];
  }

  /**
   * Return allowed values for filter type "range".
   */
  protected function getAllowedValuesForFIlterTypeRange(SelectInterface $query, EntityFilterListInterface $filters): array {
    $field_info = $this->getFieldInfo();

    if (!$field_info) {
      return [];
    }

    $query_with_filters = clone $query;

    $filters->applyConditions($query_with_filters, [$this->id()]);

    $field_table_alias = $this->joinTable($query_with_filters);
    $query_with_filters->addExpression("MIN($field_table_alias.{$field_info['column']})", 'value_min');
    $query_with_filters->addExpression("MAX($field_table_alias.{$field_info['column']})", 'value_max');
    $result = $query_with_filters->execute()->fetch();

    return [
      'min' => (float)$result->value_min,
      'max' => (float)$result->value_max,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function applyCondition(SelectInterface $query = NULL): void {
    if ($this->hasValues()) {
      $this->callMethodWithPrefix('applyConditionForFilterType', $this->getType(), ...func_get_args());
    }
  }

  /**
   * Apply condition for checkboxes.
   */
  protected function applyConditionForFilterTypeValue(SelectInterface $query): void {
      $field_table_alias = $this->joinTable($query);
      $field_info = $this->getFieldInfo();

      $query->condition("$field_table_alias.{$field_info['column']}", $this->getValues(), 'IN');
  }

  /**
   * Apply condition for checkbox.
   */
  protected function applyConditionForFilterTypeBoolean(SelectInterface $query): void {
    $field_table_alias = $this->joinTable($query);
    $field_info = $this->getFieldInfo();

    if (!empty($this->pluginDefinition['field_value'])) {
      $query->condition("$field_table_alias.{$field_info['column']}", $this->pluginDefinition['field_value']);
    }
    else {
      $query->isNotNull("$field_table_alias.{$field_info['column']}");
    }
  }

  /**
   * Apply condition for range.
   */
  protected function applyConditionForFilterTypeRange(SelectInterface $query): void {
    $field_table_alias = $this->joinTable($query);
    $field_info = $this->getFieldInfo();
    $values = $this->getValues();

    if (is_numeric($values['min'])) {
      $query->condition("$field_table_alias.{$field_info['column']}", $values['min'], '>=');
    }
    if (is_numeric($values['max'])) {
      $query->condition("$field_table_alias.{$field_info['column']}", $values['max'], '<=');
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getTableAliasesFromQuery(SelectInterface $query, string $table_name): array {
    $aliases = [];

    foreach ($query->getTables() as $table_info) {
      if ($table_info['table'] == $table_name) {
        $aliases[] = $table_info['alias'];
      }
    }

    return $aliases;
  }

  /**
   * {@inheritDoc}
   */
  public function getTableAliasFromQuery(SelectInterface $query, string $table_name, string $position = 'first'): ?string {
    $aliases = $this->getTableAliasesFromQuery($query, $table_name);

    if ($aliases) {
      if ($position == 'first') {
        return reset($aliases);
      }
      return end($aliases);
    }

    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function createTableAlias(string $table_name, string $alias_prefix = '', string $alias_suffix = ''): string {
    $replace_pattern = '[^a-z0-9_]+';
    if ($alias_prefix) {
      $alias_prefix = trim(preg_replace('/' . $replace_pattern . '/', '_', $alias_prefix), '_') . '_';
    }
    if ($alias_suffix) {
      $alias_suffix = '_' . trim(preg_replace('/' . $replace_pattern . '/', '_', $alias_suffix), '_');
    }
    return $alias_prefix . $table_name . $alias_suffix;
  }

}
