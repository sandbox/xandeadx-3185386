<?php

namespace Drupal\entity_filter_sql\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * @ViewsFilter("example_entity_filter")
 */
class ExampleSqlEntityFilter extends FilterPluginBase {

  /**
   * {@inheritDoc}
   */
  public function query() {
    $this->ensureMyTable();
    $views_query = $this->query; /** @var \Drupal\views\Plugin\views\query\Sql $views_query */

    $filter_manager = \Drupal::service('plugin.manager.sql_entity_filter');
    $current_filters = $filter_manager->getCurrentFilters();

    if ($current_filters->count() > 0) {
      $product_query = $filter_manager->getQuery('exists_subquery');

      $product_query_base_table_alias = $product_query->getMetaData('base_table_alias');
      $product_query_base_table_primary_key = $product_query->getMetaData('base_table_primary_key');
      $product_query->addField($product_query_base_table_alias, $product_query_base_table_primary_key);
      $product_query->where("$product_query_base_table_alias.$product_query_base_table_primary_key = $this->tableAlias.$product_query_base_table_primary_key");

      $current_filters->applyConditions($product_query);

      $views_query->addWhereExpression($this->options['group'], "EXISTS ($product_query)", $product_query->arguments());
    }
  }

}
