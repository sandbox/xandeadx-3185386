<?php

namespace Drupal\entity_filter_sql;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\entity_filter\EntityFilterManagerInterface;

interface SqlEntityFilterManagerInterface extends EntityFilterManagerInterface {

  /**
   * {@inheritDoc}
   */
  public function getQuery(string $alias_prefix = ''): SelectInterface;

  /**
   * Return table alias with prefix.
   */
  public function generateTableAlias(string $table_name, string $alias_prefix): string;

}
