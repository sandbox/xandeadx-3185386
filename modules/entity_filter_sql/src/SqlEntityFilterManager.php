<?php

namespace Drupal\entity_filter_sql;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\entity_filter\EntityFilterManager;

abstract class SqlEntityFilterManager extends EntityFilterManager implements SqlEntityFilterManagerInterface {

  /**
   * {@inheritDoc}
   */
  public function getQuery(string $alias_prefix = ''): SelectInterface {
    $base_table_alias = $this->generateTableAlias($this->entitySchema['table'], $alias_prefix);
    $query = $this->database->select($this->entitySchema['table'], $base_table_alias);

    $query->addMetaData('entity_type_id', $this->entityTypeId);
    $query->addMetaData('base_table', $this->entitySchema['table']);
    $query->addMetaData('base_table_alias', $base_table_alias);
    $query->addMetaData('base_table_primary_key', $this->entitySchema['primary_key']);
    $query->addMetaData('alias_prefix', $alias_prefix);

    return $query;
  }

  /**
   * {@inheritDoc}
   */
  public function generateTableAlias(string $table_name, string $alias_prefix): string {
    return ($alias_prefix ? $alias_prefix . '_' : '') . $table_name;
  }

}
