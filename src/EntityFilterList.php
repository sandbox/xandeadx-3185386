<?php

namespace Drupal\entity_filter;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\entity_filter\Plugin\EntityFilter\EntityFilterInterface;

class EntityFilterList extends \ArrayObject implements EntityFilterListInterface {

  /**
   * Class constructor.
   */
  public function __construct(array $filters = []) {
    parent::__construct();

    foreach ($filters as $filter_id => $filter) {
      if (!is_string($filter_id)) {
        $filter_id = $filter->id();
      }
      $this[$filter_id] = $filter;
    }
  }

  /**
   * Add filter.
   *
   * @param EntityFilterInterface $value
   */
  public function append(mixed $value): void {
    $this[$value->id()] = $value;
  }

  /**
   * {@inheritDoc}
   */
  public function containsFilterWithValues(string $filter_id = NULL): bool {
    if ($filter_id) {
      return isset($this[$filter_id]) && $this[$filter_id]->hasValues();
    }

    foreach ($this as $filter) {
      if ($filter->hasValues()) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function containsFiltersWithValues(array $filters_ids, string $operator = 'and'): bool {
    foreach ($filters_ids as $filter_id) {
      if ($this->containsFilterWithValues($filter_id)) {
        if ($operator == 'or') {
          return TRUE;
        }
      }
      elseif ($operator == 'and') {
        return FALSE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function getValues(): array {
    $filters_values = [];

    foreach ($this as $filter_id => $filter) {
      if ($filter->hasValues()) {
        $filters_values[$filter_id] = $filter->getValues();
      }
    }

    return $filters_values;
  }

  /**
   * {@inheritDoc}
   */
  public function sort(): self {
    $this->uasort(function (EntityFilterInterface $filter_a, EntityFilterInterface $filter_b) {
      return $filter_a->getWeight() <=> $filter_b->getWeight();
    });
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function applyConditions(SelectInterface $query, array $without = []): void {
    /** @var EntityFilterInterface $filter */
    foreach ($this as $filter_id => $filter) {
      if (!$without || !in_array($filter_id, $without)) {
        $filter->applyCondition($query);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getFiltersByIds(array $filters_ids, bool $only_has_values = FALSE): self {
    $result = new self();

    foreach ($filters_ids as $filter_id) {
      if (isset($this[$filter_id])) {
        $filter = $this[$filter_id]; /** @var EntityFilterInterface $filter */
        if (!$only_has_values || $filter->hasValues()) {
          $result[$filter_id] = $filter;
        }
      }
    }

    return $result;
  }

}
