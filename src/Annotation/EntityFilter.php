<?php

namespace Drupal\entity_filter\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * @Annotation
 */
class EntityFilter extends Plugin {

  public string $id;

  public string $type = 'value';

  public string $operator = 'or';

  public string $title = 'Filter';

  public string $description = '';

  public int $weight = 0;

  public array $field = ['path' => ''];

  public string $search_api_field = '';

  public string $field_value = '';

  public array $form_element = ['type' => 'checkboxes'];

  public bool $show_in_form = TRUE;

  public bool $global = FALSE;

  public array $category = [];

}
