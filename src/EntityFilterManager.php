<?php

namespace Drupal\entity_filter;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\DefaultTableMapping;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\druhels\ArrayHelper;
use Drupal\druhels\EntityHelper;
use Drupal\entity_filter\Annotation\EntityFilter as EntityFilterAnnotation;
use Drupal\entity_filter\Plugin\EntityFilter\EntityFilterInterface;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class EntityFilterManager extends DefaultPluginManager implements EntityFilterManagerInterface, FallbackPluginManagerInterface {

  protected Connection $database;

  protected EntityTypeManagerInterface $entityTypeManager;

  protected EntityFieldManagerInterface $entityFieldManager;

  protected SqlEntityStorageInterface $entityStorage;

  protected RequestStack $requestStack;

  /**
   * Array of EntityFilterListInterface.
   */
  protected \SplObjectStorage $currentFilters;

  public string $entityTypeId = 'node';

  public array $entitySchema = [
    'table' => NULL,
    'primary_key' => NULL,
  ];

  public string $queryParamName = 'f';

  /**
   * {@inheritDoc}
   */
  public function __construct(
    Connection $database,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    RequestStack $request_stack,
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
  ) {
    parent::__construct('Plugin/EntityFilter', $namespaces, $module_handler, EntityFilterInterface::class, EntityFilterAnnotation::class);
    $this->alterInfo('entity_filter_info');
    $this->setCacheBackend($cache_backend, 'entity_filter_plugins', ['entity_filter_plugins']);

    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->requestStack = $request_stack;
    $this->entityStorage = $this->entityTypeManager->getStorage($this->entityTypeId);

    $entityTableMapping = $this->entityStorage->getTableMapping(); /** @var DefaultTableMapping $entityTableMapping */
    $this->entitySchema['table'] = $entityTableMapping->getDataTable();
    $this->entitySchema['primary_key'] = $this->entityStorage->getEntityType()->getKey('id');

    $this->currentFilters = new \SplObjectStorage();
  }

  /**
   * {@inheritDoc}
   */
  public function createInstance($plugin_id, array $configuration = []): EntityFilterInterface {
    return parent::createInstance($plugin_id, $configuration);
  }

  /**
   * {@inheritDoc}
   */
  protected function findDefinitions(): array {
    return $this->sortDefinitions(parent::findDefinitions());
  }

  /**
   * {@inheritDoc}
   */
  public function processDefinition(&$definition, $plugin_id): void {
    parent::processDefinition($definition, $plugin_id);

    if (!empty($definition['field']['path'])) {
      // Add field info in filter definition
      $definition['field'] += [
        'name' => '',
        'property' => '',
        'table' => '',
        'column' => '',
        'type' => '',
        'multivalue' => FALSE,
      ];

      $field_parts = explode('.', $definition['field']['path']);
      $definition['field']['name'] = $field_parts[0];

      $field_storage = $this->entityFieldManager->getFieldStorageDefinitions($this->entityTypeId)[$definition['field']['name']];
      $definition['field']['type'] = $field_storage->getType();
      $definition['field']['target_type'] = $field_storage->getSetting('target_type');

      if (count($field_parts) == 2) {
        $definition['field']['property'] = $field_parts[1];
        $definition['field']['table'] = EntityHelper::getFieldDataTableName($this->entityTypeId, $definition['field']['name']);
        $definition['field']['column'] = implode('_', $field_parts);
        $definition['field']['multivalue'] = ($field_storage->getCardinality() != 1);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []): string {
    return 'broken';
  }

  /**
   * {@inheritDoc}
   */
  public function createFilter(string $filter_id, array $values = []): EntityFilterInterface {
    return $this->createInstance($filter_id, ['values' => $values]);
  }

  /**
   * {@inheritDoc}
   */
  public function createFilters(array $filters_values): EntityFilterListInterface {
    $filters_definitions = $this->getDefinitions();
    $filters_values = array_intersect_key($filters_values, $filters_definitions);
    $filters_values = ArrayHelper::sortBySecondArrayKeys($filters_values, $filters_definitions);
    $filters = new EntityFilterList();

    foreach ($filters_values as $filter_name => $filter_values) {
      if (!is_array($filter_values)) {
        $filter_values = [$filter_values];
      }
      $filters[$filter_name] = $this->createFilter($filter_name, $filter_values);
    }

    return $filters;
  }

  /**
   * {@inheritDoc}
   */
  public function sortDefinitions(array $definitions): array {
    uasort($definitions, [SortArray::class, 'sortByWeightElement']);

    return $definitions;
  }

  /**
   * {@inheritDoc}
   */
  public function getCurrentFilters(): EntityFilterListInterface {
    $request = $this->requestStack->getCurrentRequest();

    if (!isset($this->currentFilters[$request])) {
      $current_filters_values = \Drupal::request()->query->all()[$this->queryParamName] ?? [];
      $this->currentFilters[$request] = $this->createFilters($current_filters_values);
    }

    return $this->currentFilters[$request];
  }

  /**
   * {@inheritDoc}
   */
  public function removeCurrentFilters(): void {
    $request = $this->requestStack->getCurrentRequest();
    unset($this->currentFilters[$request]);
  }

}
