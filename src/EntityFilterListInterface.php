<?php

namespace Drupal\entity_filter;

use Drupal\Core\Database\Query\SelectInterface;

interface EntityFilterListInterface extends \ArrayAccess, \IteratorAggregate, \Countable, \Serializable {

  /**
   * Return TRUE if list contains filter $filter_id with values.
   * If $filter_id is empty then return TRUE if list contains any filter with values.
   */
  public function containsFilterWithValues(string $filter_id = NULL): bool;

  /**
   * Return TRUE if list contains filters with values from $filters_ids.
   */
  public function containsFiltersWithValues(array $filters_ids, string $operator = 'and'): bool;

  /**
   * Return filters values.
   */
  public function getValues(): array;

  /**
   * Sort filters.
   */
  public function sort(): self;

  /**
   * Apply filters conditions.
   */
  public function applyConditions(SelectInterface $query, array $without = []): void;

  /**
   * Return new filters list by filters ids.
   */
  public function getFiltersByIds(array $filters_ids, bool $only_has_values = FALSE): self;

  /**
   * Method from \ArrayObject.
   */
  public function append(mixed $value): void;

  /**
   * Method from \ArrayObject.
   */
  public function asort(int $flags = SORT_REGULAR): bool;

  /**
   * Method from \ArrayObject.
   */
  public function exchangeArray(array|object $array): array;

  /**
   * Method from \ArrayObject.
   */
  public function getArrayCopy(): array;

  /**
   * Method from \ArrayObject.
   */
  public function ksort(int $flags = SORT_REGULAR): bool;

  /**
   * Method from \ArrayObject.
   */
  public function natcasesort(): bool;

  /**
   * Method from \ArrayObject.
   */
  public function natsort(): bool;

  /**
   * Method from \ArrayObject.
   */
  public function uasort(callable $callback): bool;

  /**
   * Method from \ArrayObject.
   */
  public function uksort(callable $callback): bool;

}
