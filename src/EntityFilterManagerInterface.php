<?php

namespace Drupal\entity_filter;

use Drupal\entity_filter\Plugin\EntityFilter\EntityFilterInterface;

interface EntityFilterManagerInterface {

  /**
   * Return main query.
   */
  public function getQuery();

  /**
   * Create filter instance.
   */
  public function createFilter(string $filter_id, array $values = []): EntityFilterInterface;

  /**
   * Create filters instances.
   */
  public function createFilters(array $filters_values): EntityFilterListInterface;

  /**
   * Sort definitions.
   */
  public function sortDefinitions(array $definitions): array;

  /**
   * Return current filters.
   */
  public function getCurrentFilters(): EntityFilterListInterface;

  /**
   * Clear current filters.
   */
  public function removeCurrentFilters(): void;

}
