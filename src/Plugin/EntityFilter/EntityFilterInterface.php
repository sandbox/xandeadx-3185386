<?php

namespace Drupal\entity_filter\Plugin\EntityFilter;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * @property array $values
 */
interface EntityFilterInterface extends PluginInspectionInterface {

  /**
   * Return filter id.
   */
  public function id(): string;

  /**
   * Return filter title from definition.
   */
  public function getTitle(): string;

  /**
   * Return filter type.
   */
  public function getType(): string;

  /**
   * Return "operator" property from filter definition.
   */
  public function getOperator(): string;

  /**
   * Return "show_in_form" property from filter definition.
   */
  public function getShowInForm(): bool;

  /**
   * Return "category" property from filter definition.
   */
  public function getCategory(): array;

  /**
   * Return "global" property from filter definition.
   */
  public function isGlobal(): bool;

  /**
   * Return "weight" property from filter definition.
   */
  public function getWeight(): int;

  /**
   * Return filter values.
   */
  public function getValues(): array;

  /**
   * Return first filter value.
   */
  public function getFirstValue(): mixed;

  /**
   * Set filter values.
   */
  public function setValues(array $values): void;

  /**
   * Return TRUE if filter hav values.
   */
  public function hasValues(): bool;

  /**
   * Return field info from definition.
   */
  public function getFieldInfo(string $key = NULL, mixed $default = NULL): mixed;

  /**
   * Add to query condition by filter values.
   */
  public function applyCondition(): void;

  /**
   * Preprocess condition values.
   */
  public function processConditionValues(array $values): array;

  /**
   * Return allowed values.
   */
  public function getAllowedValues(): array;

  /**
   * Format allowed values.
   */
  public function formatAllowedValues(array $allowed_values): array;

  /**
   * Return form element info.
   */
  public function getFormElementInfo(string $key = NULL, mixed $default = NULL): mixed;

  /**
   * Return form element.
   */
  public function getFormElement(array $allowed_values): array;

  /**
   * Convert form element values to filter values.
   */
  public function massageFormValues($form_values): array;

}
