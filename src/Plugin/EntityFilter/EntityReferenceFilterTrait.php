<?php

namespace Drupal\entity_filter\Plugin\EntityFilter;

use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermInterface;

trait EntityReferenceFilterTrait {

  /**
   * Return value entities.
   *
   * @return TermInterface[]
   */
  public function getValueEntities(): array {
    if ($this->hasValues()) {
      return Term::loadMultiple($this->getValues());
    }
    return [];
  }

  /**
   * Return first brand entity from values.
   */
  public function getValueEntity(): ?TermInterface {
    if ($this->hasValues()) {
      return Term::load($this->getFirstValue());
    }
    return NULL;
  }

}
