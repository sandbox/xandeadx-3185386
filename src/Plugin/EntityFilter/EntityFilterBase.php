<?php

namespace Drupal\entity_filter\Plugin\EntityFilter;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Render\Element\Checkboxes;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class EntityFilterBase extends PluginBase implements EntityFilterInterface, ContainerFactoryPluginInterface {

  public array $values = [];

  public EntityTypeManagerInterface $entityTypeManager;

  public EntityFieldManagerInterface $entityFieldManager;

  /**
   * Class constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;

    if (isset($configuration['values'])) {
      //$this->values = $configuration['values'];
      $this->setValues($configuration['values']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function id(): string {
    return $this->getPluginId();
  }

  /**
   * {@inheritDoc}
   */
  public function getTitle(): string {
    return $this->pluginDefinition['title'];
  }

  /**
   * {@inheritDoc}
   */
  public function getType(): string {
    return $this->pluginDefinition['type'];
  }

  /**
   * {@inheritDoc}
   */
  public function getOperator(): string {
    return $this->pluginDefinition['operator'];
  }

  /**
   * {@inheritDoc}
   */
  public function getShowInForm(): bool {
    return (bool)$this->pluginDefinition['show_in_form'];
  }

  /**
   * {@inheritDoc}
   */
  public function getCategory(): array {
    return $this->pluginDefinition['category'];
  }

  /**
   * {@inheritDoc}
   */
  public function isGlobal(): bool {
    return $this->pluginDefinition['global'];
  }

  /**
   * {@inheritDoc}
   */
  public function getWeight(): int {
    return $this->pluginDefinition['weight'];
  }

  /**
   * {@inheritDoc}
   */
  public function getValues(): array {
    return $this->values;
  }

  /**
   * {@inheritDoc}
   */
  public function getFirstValue(): mixed {
    return current($this->values);
  }

  /**
   * {@inheritDoc}
   */
  public function setValues(array $values): void {
    $this->values = $values;
  }

  /**
   * {@inheritDoc}
   */
  public function hasValues(): bool {
    $values = $this->getValues();
    $filter_type = $this->getType();

    if ($filter_type == 'value') {
      return count($values) > 0;
    }
    if ($filter_type == 'range') {
      return
        $values &&
        (
          (isset($values['min']) && is_numeric($values['min'])) ||
          (isset($values['max']) && is_numeric($values['max']))
        );
    }

    return (bool)$values;
  }

  /**
   * {@inheritDoc}
   */
  public function getFieldInfo(string $key = NULL, mixed $default = NULL): mixed {
    if ($key) {
      return $this->pluginDefinition['field'][$key] ?? $default;
    }
    return $this->pluginDefinition['field'] ?? [];
  }

  /**
   * {@inheritDoc}
   */
  public function getAllowedValues(): array {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function formatAllowedValues(array $allowed_values): array {
    $allowed_values_titles = [];

    // @TODO Move to SqlEntityFilterBase
    /*$field_info = $this->getFieldInfo();
    if ($field_info) {
      if ($field_info['type'] == 'entity_reference') {
        if ($field_info['target_type'] == 'taxonomy_term') {
          $allowed_values_titles = TaxonomyHelper::getTermsName(array_keys($allowed_values), FALSE);
        }
      }
    }*/

    foreach ($allowed_values as $value => $counter) {
      $allowed_values[$value] = ($allowed_values_titles[$value] ?? $value) . ' <span class="counter">' . $counter . '</span>';
    }

    return $allowed_values;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormElementInfo(string $key = NULL, mixed $default = NULL): mixed {
    if ($key) {
      return $this->pluginDefinition['form_element'][$key] ?? $default;
    }
    return $this->pluginDefinition['form_element'];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormElement(array $allowed_values): array {
    $element = $this->callMethodWithPrefix('getFormElement', $this->getFormElementInfo('type'), ...func_get_args());

    if ($element) {
      $element['#entity_filter'] = $this;
    }

    return $element;
  }

  /**
   * Return checkboxes form element.
   */
  protected function getFormElementCheckboxes(array $allowed_values): array {
    $element = [];

    if ($allowed_values) {
      $element = [
        '#type' => 'checkboxes',
        '#title' => $this->getTitle(),
        '#options' => $this->formatAllowedValues($allowed_values),
        '#default_value' => $this->getValues(),
      ];

      foreach ($allowed_values as $allowed_value => $counter) {
        if ($counter == 0 && !in_array($allowed_value, $this->getValues())) {
          $element[$allowed_value]['#disabled'] = TRUE;
        }
      }
    }

    return $element;
  }

  /**
   * Return checkboxes form element.
   */
  protected function getFormElementCheckbox(array $allowed_values): array {
    $return_value = $this->getFormElementInfo('return_value', 1);

    if (isset($allowed_values[$return_value]) || $this->hasValues()) {
      $element_default_value = (bool)$this->getFirstValue();

      return [
        '#type' => 'checkbox',
        '#title' => $this->getTitle() . ' <span class="counter">' . ($allowed_values[$return_value] ?? 0) . '</span>',
        '#default_value' => $element_default_value,
        '#disabled' => empty($allowed_values[$return_value]) && !$element_default_value,
        '#return_value' => $return_value,
      ];
    }

    return [];
  }

  /**
   * Return radios form element.
   */
  protected function getFormElementRadios(array $allowed_values): array {
    $element = $this->getFormElementCheckboxes($allowed_values);

    if ($element) {
      $element['#type'] = 'radios';
      $element['#default_value'] = $this->getFirstValue();
    }

    return $element;
  }

  /**
   * Return range form element.
   */
  protected function getFormElementRange(array $allowed_values): array {
    $element = [];

    if (isset($allowed_values['min']) || isset($allowed_values['max']) || $this->hasValues()) {
      $values = $this->getValues();
      $element_info = $this->getFormElementInfo();

      $element = [
        '#type' => 'range_slider',
        '#title' => $this->getTitle(),
        '#min' => $allowed_values['min'] ?? NULL,
        '#max' => $allowed_values['max'] ?? NULL,
        '#step' => $element_info['step'] ?? 1,
        '#validate_numbers' => FALSE,
        '#default_value' => [
          'min' => $values['min'] ?? NULL,
          'max' => $values['max'] ?? NULL,
        ],
      ];
    }

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function massageFormValues($form_values): array {
    $form_element_type = $this->getFormElementInfo('type');

    if ($form_element_type == 'checkboxes') {
      return Checkboxes::getCheckedCheckboxes($form_values);
    }
    if ($form_element_type == 'checkbox') {
      return $form_values ? [$form_values] : [];
    }
    if ($form_element_type == 'radios') {
      return $form_values ? [$form_values] : [];
    }
    if ($form_element_type == 'range') {
      return [
        'min' => (isset($form_values['min']) && is_numeric($form_values['min'])) ? (float)$form_values['min'] : NULL,
        'max' => (isset($form_values['max']) && is_numeric($form_values['max'])) ? (float)$form_values['max'] : NULL,
      ];
    }

    return $form_values;
  }

  /**
   * {@inheritDoc}
   */
  public function applyCondition(): void { }

  /**
   * {@inheritDoc}
   */
  public function processConditionValues(array $values): array {
    return $values;
  }

  /**
   * Start timer.
   */
  protected function timerStart(&$time): void {
    $time = microtime(TRUE);
  }

  /**
   * Stop timer
   */
  protected function timerStop(&$time): void {
    $time = round((microtime(TRUE) - $time) * 1000, 2);
  }

  /**
   * Call method with prefix.
   */
  protected function callMethodWithPrefix(string $prefix, string $method, ...$arguments) {
    $method = $prefix . Container::camelize($method);
    if (method_exists($this, $method)) {
      return $this->{$method}(...$arguments);
    }
  }

}
